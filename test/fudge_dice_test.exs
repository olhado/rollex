defmodule FudgeDiceTest do
  use ExUnit.Case

  alias Rollex.Tokens.FudgeDice

  def from_string(string) do
    Rollex.Token.create(
      %FudgeDice{},
      string,
      Regex.run(%FudgeDice{}.regex, string, return: :index)
    )
  end

  def only_token_from_string(string) do
    string
    |> from_string()
    |> elem(0)
  end

  test "Creating" do
    assert(
      match?(
        {%FudgeDice{quantity: 4}, 3},
        from_string("4df")
      )
    )
  end

  test "Min" do
    assert(Rollex.Dice.min(only_token_from_string("4df")) == -4)
  end

  test "Max" do
    assert(Rollex.Dice.max(only_token_from_string("4df")) == 4)
  end

  test "Rolling" do
    {:ok, roll} = Rollex.roll("10df")

    assert(
      match?(
        %{
          tokens: [
            %FudgeDice{
              quantity: 10
            }
            | _
          ],
          totals: %{
            arithmetic: _,
            successes: _
          }
        },
        roll
      )
    )
  end
end
