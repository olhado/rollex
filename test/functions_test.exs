defmodule FunctionsTest do
  use ExUnit.Case

  alias Rollex.Tokens.LeftParenthesis

  def from_string(string) do
    Rollex.Token.create(
      %LeftParenthesis{},
      string,
      Regex.run(%LeftParenthesis{}.regex, string, return: :index)
    )
  end

  test "Creating" do
    fun = &ceil/1

    assert(
      match?(
        {%LeftParenthesis{function: ^fun}, 5},
        from_string("ceil(4d4)")
      )
    )
  end

  test "ceil" do
    [
      {"ceil(0.75)", 1},
      {"ceil(0.5)", 1},
      {"ceil(0.05)", 1},
      {"ceil(1.9)", 2}
    ]
    |> Enum.each(fn {roll, result} ->
      assert(
        match?(
          {:ok, %{totals: %{arithmetic: ^result}}},
          Rollex.roll(roll)
        )
      )
    end)
  end

  test "floor" do
    [
      {"floor(0.75)", 0},
      {"floor(0.5)", 0},
      {"floor(0.05)", 0},
      {"floor(1.9)", 1}
    ]
    |> Enum.each(fn {roll, result} ->
      assert(
        match?(
          {:ok, %{totals: %{arithmetic: ^result}}},
          Rollex.roll(roll)
        )
      )
    end)
  end

  test "round" do
    [
      {"round(0.75)", 1},
      {"round(0.5)", 1},
      {"round(0.05)", 0},
      {"round(1.9)", 2}
    ]
    |> Enum.each(fn {roll, result} ->
      assert(
        match?(
          {:ok, %{totals: %{arithmetic: ^result}}},
          Rollex.roll(roll)
        )
      )
    end)
  end

  test "unknown functions cause errors" do
    [
      "unknown(0.75)",
      "1d4 * florp(5)"
    ]
    |> Enum.each(fn roll ->
      assert(
        match?(
          {:error, _},
          Rollex.roll(roll)
        )
      )
    end)
  end
end
