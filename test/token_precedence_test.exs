defmodule TokenPrecedenceTest do
  use ExUnit.Case

  test "precedence is as expected" do
    correctly_ordered = [
      %Rollex.Tokens.Multiplication{},
      %Rollex.Tokens.Division{},
      %Rollex.Tokens.Subtraction{},
      %Rollex.Tokens.Addition{},
      %Rollex.Tokens.RightParenthesis{},
      %Rollex.Tokens.RegularDice{},
      %Rollex.Tokens.Number{},
      %Rollex.Tokens.LeftParenthesis{},
      %Rollex.Tokens.Histogram{},
      %Rollex.Tokens.FudgeDice{},
      %Rollex.Tokens.End{}
    ]

    sorted_by_precedence =
      Enum.sort(
        correctly_ordered,
        fn left, right ->
          r_precedence = Rollex.Token.lbp(right)

          case Rollex.Token.lbp(left) do
            l_precedence when l_precedence == r_precedence ->
              left.__struct__ > right.__struct__

            l_precedence ->
              l_precedence > r_precedence
          end
        end
      )

    assert(correctly_ordered == sorted_by_precedence)
  end
end
