defmodule TokenCreationTest do
  use ExUnit.Case

  test "create default number" do
    assert %Rollex.Tokens.Number{} == %Rollex.Tokens.Number{value: 0.0}
  end

  test "create number 1" do
    assert Rollex.Token.create(%Rollex.Tokens.Number{}, "1", [{0, 1}]) ==
             {%Rollex.Tokens.Number{value: 1.0}, 1}
  end

  test "create number 300.0501" do
    assert Rollex.Token.create(%Rollex.Tokens.Number{}, "300.0501", [{0, 8}]) ==
             {%Rollex.Tokens.Number{value: 300.0501}, 8}
  end

  test "create End token" do
    assert Rollex.Token.create(%Rollex.Tokens.End{}, "", []) == {%Rollex.Tokens.End{}, 0}
  end

  test "create 1d8" do
    regular_dice = %Rollex.Tokens.RegularDice{}
    expr = "1d8"

    assert Rollex.Token.create(
             regular_dice,
             expr,
             Regex.run(regular_dice.regex, expr, return: :index)
           ) ==
             {%Rollex.Tokens.RegularDice{quantity: 1, sides: 8}, 3}
  end

  test "create d8" do
    regular_dice = %Rollex.Tokens.RegularDice{}
    expr = "d8"

    assert Rollex.Token.create(
             regular_dice,
             expr,
             Regex.run(regular_dice.regex, expr, return: :index)
           ) ==
             {%Rollex.Tokens.RegularDice{quantity: 1, sides: 8}, 2}
  end

  test "create 333d99999" do
    regular_dice = %Rollex.Tokens.RegularDice{}
    expr = "333d99999"

    assert Rollex.Token.create(
             regular_dice,
             expr,
             Regex.run(regular_dice.regex, expr, return: :index)
           )

    {%Rollex.Tokens.RegularDice{quantity: 333, sides: 99999}, 9}
  end

  test "create +" do
    assert Rollex.Token.create(%Rollex.Tokens.Addition{}, "+", [{0, 1}]) ==
             {%Rollex.Tokens.Addition{}, 1}
  end
end
