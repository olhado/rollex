defmodule LexerTest do
  use ExUnit.Case

  test "tokenize empty string" do
    assert(
      Rollex.Lexer.tokenize("", Rollex.default_tokens()) ==
        {
          :ok,
          [%Rollex.Tokens.End{regex: ~r/\G\z/}]
        }
    )
  end

  test "verify that whitespace stripping works" do
    assert(
      Rollex.Lexer.tokenize(" 60   d 100     ", Rollex.default_tokens()) ==
        {
          :ok,
          [
            %Rollex.Tokens.RegularDice{
              arithmetic: 0.0,
              is_dice: true,
              operation: nil,
              quantity: 60,
              regex: ~r/\G(\d*)[dD](\d+)/,
              rejected_rolls: [],
              sides: 100,
              valid_rolls: []
            },
            %Rollex.Tokens.End{regex: ~r/\G\z/}
          ]
        }
    )
  end

  test "tokenize 1" do
    assert(
      Rollex.Lexer.tokenize("1", Rollex.default_tokens()) ==
        {
          :ok,
          [
            %Rollex.Tokens.Number{regex: ~r/\G\d+(\.\d+)?/, value: 1.0},
            %Rollex.Tokens.End{regex: ~r/\G\z/}
          ]
        }
    )
  end

  test "tokenize 98.334" do
    assert(
      Rollex.Lexer.tokenize("98.334", Rollex.default_tokens()) ==
        {
          :ok,
          [
            %Rollex.Tokens.Number{
              regex: ~r/\G\d+(\.\d+)?/,
              value: 98.334
            },
            %Rollex.Tokens.End{regex: ~r/\G\z/}
          ]
        }
    )
  end

  test "tokenize 1d8" do
    assert(
      Rollex.Lexer.tokenize("1d8", Rollex.default_tokens()) ==
        {
          :ok,
          [
            %Rollex.Tokens.RegularDice{
              arithmetic: 0.0,
              is_dice: true,
              operation: nil,
              quantity: 1,
              regex: ~r/\G(\d*)[dD](\d+)/,
              rejected_rolls: [],
              sides: 8,
              valid_rolls: []
            },
            %Rollex.Tokens.End{regex: ~r/\G\z/}
          ]
        }
    )
  end

  test "tokenize d10" do
    assert(
      Rollex.Lexer.tokenize("d10", Rollex.default_tokens()) ==
        {
          :ok,
          [
            %Rollex.Tokens.RegularDice{
              arithmetic: 0.0,
              is_dice: true,
              operation: nil,
              quantity: 1,
              regex: ~r/\G(\d*)[dD](\d+)/,
              rejected_rolls: [],
              sides: 10,
              valid_rolls: []
            },
            %Rollex.Tokens.End{regex: ~r/\G\z/}
          ]
        }
    )
  end

  test "tokenize 60d400" do
    assert(
      Rollex.Lexer.tokenize("60d400", Rollex.default_tokens()) ==
        {
          :ok,
          [
            %Rollex.Tokens.RegularDice{
              arithmetic: 0.0,
              is_dice: true,
              operation: nil,
              quantity: 60,
              regex: ~r/\G(\d*)[dD](\d+)/,
              rejected_rolls: [],
              sides: 400,
              valid_rolls: []
            },
            %Rollex.Tokens.End{regex: ~r/\G\z/}
          ]
        }
    )
  end

  test "tokenize +" do
    assert(
      Rollex.Lexer.tokenize("+", Rollex.default_tokens()) ==
        {
          :ok,
          [
            %Rollex.Tokens.Addition{regex: ~r/\G\+/},
            %Rollex.Tokens.End{regex: ~r/\G\z/}
          ]
        }
    )
  end

  test "tokenize d80+33.333" do
    assert(
      Rollex.Lexer.tokenize("d80+33.333", Rollex.default_tokens()) ==
        {
          :ok,
          [
            %Rollex.Tokens.RegularDice{
              arithmetic: 0.0,
              is_dice: true,
              operation: nil,
              quantity: 1,
              regex: ~r/\G(\d*)[dD](\d+)/,
              rejected_rolls: [],
              sides: 80,
              valid_rolls: []
            },
            %Rollex.Tokens.Addition{regex: ~r/\G\+/},
            %Rollex.Tokens.Number{
              regex: ~r/\G\d+(\.\d+)?/,
              value: 33.333
            },
            %Rollex.Tokens.End{regex: ~r/\G\z/}
          ]
        }
    )
  end
end
