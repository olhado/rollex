defmodule DiceOperationsTest do
  use ExUnit.Case

  def is_even?(x), do: Integer.mod(x, 2) == 0

  test "only odd dice rolls" do
    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("10d6o")

    Enum.each(
      rejected,
      fn x ->
        assert(x > 0)
        assert(is_even?(x))
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x > 0)
        refute(is_even?(x))
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "only even dice rolls" do
    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("10d6e")

    Enum.each(
      rejected,
      fn x ->
        assert(x > 0)
        refute(is_even?(x))
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x > 0)
        assert(is_even?(x))
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take high rolls with k" do
    num_dice = 10
    keep = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6k#{keep}")

    assert(Enum.count(valid) == keep)
    assert(Enum.count(rejected) == num_dice - keep)

    low = Enum.min(valid)

    Enum.each(
      rejected,
      fn x ->
        assert(x <= low)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x >= low)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take high rolls with kh" do
    num_dice = 10
    keep = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6kh#{keep}")

    assert(Enum.count(valid) == keep)
    assert(Enum.count(rejected) == num_dice - keep)

    low = Enum.min(valid)

    Enum.each(
      rejected,
      fn x ->
        assert(x <= low)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x >= low)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take low rolls" do
    num_dice = 10
    keep = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6kl#{keep}")

    assert(Enum.count(valid) == keep)
    assert(Enum.count(rejected) == num_dice - keep)

    high = Enum.max(valid)

    Enum.each(
      rejected,
      fn x ->
        assert(x >= high)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x <= high)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "drop low rolls with d" do
    num_dice = 10
    drop = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6d#{drop}")

    assert(Enum.count(valid) == num_dice - drop)
    assert(Enum.count(rejected) == drop)

    min = Enum.min(valid)

    Enum.each(
      rejected,
      fn x ->
        assert(x <= min)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x >= min)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "drop low rolls with dl" do
    num_dice = 10
    drop = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6dl#{drop}")

    assert(Enum.count(valid) == num_dice - drop)
    assert(Enum.count(rejected) == drop)

    min = Enum.min(valid)

    Enum.each(
      rejected,
      fn x ->
        assert(x <= min)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x >= min)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "drop high rolls" do
    num_dice = 10
    drop = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6dh#{drop}")

    assert(Enum.count(valid) == num_dice - drop)
    assert(Enum.count(rejected) == drop)

    max = Enum.max(valid)

    Enum.each(
      rejected,
      fn x ->
        assert(x >= max)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x <= max)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "dropping too many results in no valid values" do
    num_dice = 5

    assert(
      match?(
        {:ok, %{tokens: [%{valid_rolls: []} | _], totals: %{arithmetic: 0, successes: 0}}},
        Rollex.roll("#{num_dice}d6d#{num_dice + 1}")
      )
    )

    assert(
      match?(
        {:ok, %{tokens: [%{valid_rolls: []} | _], totals: %{arithmetic: 0, successes: 0}}},
        Rollex.roll("#{num_dice}d6dh#{num_dice + 1}")
      )
    )
  end

  test "take rolls under a number" do
    num_dice = 10
    under = 4

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total, successes: successes}
      }
    } = Rollex.roll("#{num_dice}d6<#{under}")

    assert(Enum.count(valid) == successes)

    Enum.each(
      rejected,
      fn x ->
        assert(x >= under)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x < under)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take rolls over a number" do
    num_dice = 10
    over = 4

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total, successes: successes}
      }
    } = Rollex.roll("#{num_dice}d6>#{over}")

    assert(Enum.count(valid) == successes)

    Enum.each(
      rejected,
      fn x ->
        assert(x <= over)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x > over)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take rolls that equal a number" do
    num_dice = 10
    match = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6=#{match}")

    Enum.each(
      rejected,
      fn x ->
        refute(x == match)
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(x == match)
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take rolls that match a set" do
    num_dice = 10
    match = [2, 3, 4]

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6=[#{Enum.join(match, ",")}]")

    Enum.each(
      rejected,
      fn x ->
        refute(Enum.member?(match, x))
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(Enum.member?(match, x))
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take rolls that match a range" do
    num_dice = 10
    match = [2, 3, 4]

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6=[2..4]")

    Enum.each(
      rejected,
      fn x ->
        refute(Enum.member?(match, x))
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(Enum.member?(match, x))
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take rolls that match a reverse range" do
    num_dice = 10
    match = [2, 3, 4]

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6=[4..2]")

    Enum.each(
      rejected,
      fn x ->
        refute(Enum.member?(match, x))
      end
    )

    Enum.each(
      valid,
      fn x ->
        assert(Enum.member?(match, x))
      end
    )

    assert(total == Enum.sum(valid))
  end

  test "take all rolls when match set includes all numbers" do
    num_dice = 10
    match = [1, 2, 3, 4, 5, 6]

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6=[#{Enum.join(match, ",")}]")

    assert(Enum.empty?(rejected))
    assert(total == Enum.sum(valid))
  end

  test "reroll low rolls" do
    num_dice = 10
    target = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6r<#{target}")

    assert(Enum.reject(rejected, fn x -> x < target end) |> Enum.empty?())
    assert(total == Enum.sum(valid))
  end

  test "reroll high rolls" do
    num_dice = 10
    target = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6r>#{target}")

    assert(Enum.reject(rejected, fn x -> x > target end) |> Enum.empty?())
    assert(total == Enum.sum(valid))
  end

  test "reroll rolls equal to a number" do
    num_dice = 100
    target = 3

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6r=#{target}")

    assert(Enum.reject(rejected, fn x -> x == target end) |> Enum.empty?())
    assert(total == Enum.sum(valid))
  end

  test "reroll rolls equal to a list of numbers" do
    num_dice = 100
    target = [1, 3, 5]

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6r=[#{Enum.join(target, ",")}]")

    assert(Enum.reject(rejected, fn x -> Enum.member?(target, x) end) |> Enum.empty?())
    assert(total == Enum.sum(valid))
  end

  test "reroll rolls equal to a range of numbers" do
    num_dice = 100
    target = 3..5

    {
      :ok,
      %{
        tokens: [
          %{
            rejected_rolls: rejected,
            valid_rolls: valid
          }
          | _
        ],
        totals: %{arithmetic: total}
      }
    } = Rollex.roll("#{num_dice}d6r=[#{inspect(target)}]")

    assert(Enum.reject(rejected, fn x -> Enum.member?(target, x) end) |> Enum.empty?())
    assert(total == Enum.sum(valid))
  end

  test "! is the exploding operator" do
    %{compiled: [%{operation: operation} | _]} = Rollex.compile("1d10!")
    assert operation == :explode
  end

  test "exploding dice does not apply to dice with fewer than 2 sides" do
    %{compiled: [token | _]} = Rollex.compile("1d0!")
    assert Rollex.Dice.Operations.apply(token, [0]) == {[0], []}

    %{compiled: [token | _]} = Rollex.compile("1d1!")
    assert Rollex.Dice.Operations.apply(token, [1]) == {[1], []}
  end

  test "exploding dice match number of sides on dice" do
    %{compiled: [token | _]} = Rollex.compile("3d6!")
    {valid, rejected} = Rollex.Dice.Operations.apply(token, [1, 6, 4])
    assert Enum.count(valid) > 3
    assert rejected == []

    {valid, rejected} = Rollex.Dice.Operations.apply(token, [1, 6, 6])
    assert Enum.count(valid) > 4
    assert rejected == []
  end

  test "successes are cummulative, arithmetic is a sum" do
    {
      :ok,
      %{
        tokens: tokens,
        totals: %{arithmetic: total, successes: successes}
      }
    } = Rollex.roll("10d6<4+1d4")

    {expected_total, expected_success} =
      Enum.reduce(
        tokens,
        {0, 0},
        fn
          %{valid_rolls: rolls}, {total, success} ->
            {total + Enum.sum(rolls), success + Enum.count(rolls)}

          _, acc ->
            acc
        end
      )

    assert(expected_total == total)
    assert(expected_success == successes)
  end

  test "no successes leaves arithmetic at 0" do
    assert(
      match?(
        {
          :ok,
          %{
            totals: %{arithmetic: 0, successes: 0}
          }
        },
        Rollex.roll("10d6>6")
      )
    )
  end

  test "applying a 'nil' noop operation just returns the rolls as-is" do
    rolls = [3, 1, 4, 1, 5, 9]
    assert(Rollex.Dice.Operations.apply(%{operation: nil}, rolls) == {rolls, []})
  end

  test "malformed dice ops result in errors" do
    # filter numbers/range
    assert(match?({:error, _}, Rollex.roll("10d6=[]")))
    assert(match?({:error, _}, Rollex.roll("10d6=[a]")))
    assert(match?({:error, _}, Rollex.roll("10d6=a")))
    assert(match?({:error, _}, Rollex.roll("10d6=")))

    # keep high
    assert(match?({:error, _}, Rollex.roll("10d6kh")))
    assert(match?({:error, _}, Rollex.roll("10d6kh!")))
    assert(match?({:error, _}, Rollex.roll("10d6kh6d5")))
    assert(match?({:error, _}, Rollex.roll("10d6k")))
    assert(match?({:error, _}, Rollex.roll("10d6k!")))
    assert(match?({:error, _}, Rollex.roll("10d6k6d5")))

    # keep low
    assert(match?({:error, _}, Rollex.roll("10d6kl")))
    assert(match?({:error, _}, Rollex.roll("10d6kl!")))
    assert(match?({:error, _}, Rollex.roll("10d6kl6d5")))

    # drop high
    assert(match?({:error, _}, Rollex.roll("10d6dh")))
    assert(match?({:error, _}, Rollex.roll("10d6dh!")))
    assert(match?({:error, _}, Rollex.roll("10d6dh6d5")))

    # drop low
    assert(match?({:error, _}, Rollex.roll("10d6dl")))
    assert(match?({:error, _}, Rollex.roll("10d6dl!")))
    assert(match?({:error, _}, Rollex.roll("10d6dl6d5")))
    assert(match?({:error, _}, Rollex.roll("10d6d")))
    assert(match?({:error, _}, Rollex.roll("10d6d!")))
    assert(match?({:error, _}, Rollex.roll("10d6d6d5")))

    # filter over
    assert(match?({:error, _}, Rollex.roll("10d6>")))
    assert(match?({:error, _}, Rollex.roll("10d6>!")))
    assert(match?({:error, _}, Rollex.roll("10d6>6d5")))

    # filter under
    assert(match?({:error, _}, Rollex.roll("10d6<")))
    assert(match?({:error, _}, Rollex.roll("10d6<!")))
    assert(match?({:error, _}, Rollex.roll("10d6<6d5")))
  end
end
