defprotocol Rollex.Histogram do
  @type histogram :: %{(result :: integer) => percent_odds :: float}

  @doc """
  Returns a map containing the odds for every possible result for a token
  """
  @spec evaluate(token :: map, current_effort :: non_neg_integer) ::
          {histogram, effort_count :: non_neg_integer}
  def evaluate(token, current_effort)
end
