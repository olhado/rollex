defprotocol Rollex.Dice do
  @spec roll(token :: map) :: map
  def roll(token)

  @spec min(token :: map) :: integer
  def min(token)

  @spec max(token :: map) :: integer
  def max(token)
end
