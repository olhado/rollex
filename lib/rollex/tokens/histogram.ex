defmodule Rollex.Tokens.Histogram do
  @moduledoc false

  defstruct histogram: %{}, regex: ~r//

  @type t :: %__MODULE__{}

  defimpl Rollex.Token, for: Rollex.Tokens.Histogram do
    @left_binding_precedence 0

    def create(_token, _roll_expr, _matches) do
      {%Rollex.Tokens.Histogram{}, 0}
    end

    def nud(token, rest) do
      {:ok, %{histogram: token.histogram}, rest}
    end

    def led(_token, _left, _rest) do
      {:error, "Unexpected histogram token"}
    end

    def lbp(_token) do
      @left_binding_precedence
    end
  end
end
