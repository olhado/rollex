defmodule Rollex.Tokens.RightParenthesis do
  @moduledoc false

  @regex ~r/\G\)/
  defstruct regex: @regex

  @type t :: %__MODULE__{}

  defimpl Rollex.Token, for: Rollex.Tokens.RightParenthesis do
    @left_binding_precedence 0

    def create(_token, _roll_expr, _matches) do
      {%Rollex.Tokens.RightParenthesis{}, 1}
    end

    def nud(_token, _rest) do
      {:error, "Unexpected closing parenthesis"}
    end

    def led(_token, _left, _rest) do
      {:error, "Unexpected closing parenthesis"}
    end

    def lbp(_token) do
      @left_binding_precedence
    end
  end
end
