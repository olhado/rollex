defmodule Rollex.Tokens.LeftParenthesis do
  @moduledoc false

  @regex ~r/\G(\w*)\(/
  defstruct regex: @regex, function: nil

  @type t :: %__MODULE__{}

  defimpl Rollex.Token, for: Rollex.Tokens.LeftParenthesis do
    @left_binding_precedence 0

    def create(_token, roll_expr, [_, {fn_name_start, fn_name_length}]) when fn_name_length > 0 do
      case function_for_string(String.slice(roll_expr, fn_name_start, fn_name_length)) do
        {:error, _} = error -> error
        function -> {%Rollex.Tokens.LeftParenthesis{function: function}, fn_name_length + 1}
      end
    end

    def create(_token, _roll_expr, _matches) do
      {%Rollex.Tokens.LeftParenthesis{}, 1}
    end

    def nud(token, rest) do
      case Rollex.PrattParser.evaluate_expression(rest, @left_binding_precedence) do
        {:ok, left, [%Rollex.Tokens.RightParenthesis{} | rest]} ->
          {:ok, apply_function(token, left), rest}

        {:ok, _left, _rest} ->
          {:error, "Missing closing parenthesis"}

        result ->
          result
      end
    end

    def led(_token, _left, _rest) do
      {:error, "Unexpected opening parenthesis"}
    end

    def lbp(_token) do
      @left_binding_precedence
    end

    defp apply_function(%{function: func}, %{arithmetic: value} = result) when func != nil do
      Map.put(result, :arithmetic, func.(value))
    end

    defp apply_function(_, result), do: result

    defp function_for_string("floor"), do: &floor/1
    defp function_for_string("ceil"), do: &ceil/1
    defp function_for_string("round"), do: &round/1

    defp function_for_string(unknown_function),
      do: {:error, "Unknown function: #{unknown_function}"}
  end
end
