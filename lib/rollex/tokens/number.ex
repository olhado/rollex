defmodule Rollex.Tokens.Number do
  @moduledoc false

  @regex ~r/\G\d+(\.\d+)?/
  defstruct regex: @regex, value: 0.0

  @type t :: %__MODULE__{}

  defimpl Rollex.Token, for: Rollex.Tokens.Number do
    @left_binding_precedence 0

    def create(_token, roll_expr, [{number_start, number_end} | _]) do
      number_string = String.slice(roll_expr, number_start, number_end)

      {
        %Rollex.Tokens.Number{
          value: Rollex.Utilities.simple_float_parse(number_string)
        },
        number_end
      }
    end

    def nud(token, rest) do
      {:ok, %{arithmetic: token.value}, rest}
    end

    def led(_token, _left, _rest) do
      {:error, "Unexpected numeric value"}
    end

    def lbp(_token) do
      @left_binding_precedence
    end
  end
end
