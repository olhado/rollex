defmodule Rollex.Tokens.Multiplication do
  @moduledoc false

  @regex ~r/\G\*/
  defstruct regex: @regex

  @type t :: %__MODULE__{}

  defimpl Rollex.Token, for: Rollex.Tokens.Multiplication do
    @left_binding_precedence 20

    def create(_token, _roll_expr, _matches) do
      {%Rollex.Tokens.Multiplication{}, 1}
    end

    def nud(_token, _rest) do
      {:error, "Unexpected '*' sign in input"}
    end

    def led(token, left, rest) do
      Rollex.PrattParser.evaluate_expression(rest, Rollex.Token.lbp(token))
      |> perform_led(left)
    end

    defp merge_op(k, l, r) do
      case k do
        :arithmetic ->
          l * r

        _ ->
          # Just add non-arithmetic items
          l + r
      end
    end

    def lbp(_token) do
      @left_binding_precedence
    end

    defp perform_led({:ok, right, new_rest}, left) do
      multiplied = Rollex.Utilities.merge(left, right, &merge_op/3)
      {:ok, multiplied, new_rest}
    end

    defp perform_led(bad_result, _left), do: bad_result
  end
end
