defmodule Rollex.Lexer do
  @moduledoc false

  @spec tokenize(input :: String.t(), list) ::
          {:error, reason :: String.t()} | {:ok, tokens :: [map]}
  def tokenize(_input, []) do
    {:error, "No valid list of tokens provided!"}
  end

  def tokenize(input, valid_tokens) when is_binary(input) and is_list(valid_tokens) do
    spaceless = input |> String.replace(~r/\s/, "")
    _do_tokenize(spaceless, valid_tokens, String.length(spaceless), 0, [])
  end

  def tokenize(_input, _valid_tokens) do
    {:error, "Invalid input provied"}
  end

  defp _do_tokenize(_roll_expr, _valid_tokens, roll_expr_length, roll_expr_offset, current_result)
       when roll_expr_length <= roll_expr_offset do
    {:ok, Enum.reverse([%Rollex.Tokens.End{} | current_result])}
  end

  defp _do_tokenize(roll_expr, valid_tokens, roll_expr_length, roll_expr_offset, result) do
    case _match_and_create(roll_expr, roll_expr_offset, valid_tokens) do
      {:error, _message} = error ->
        error

      {token, length} ->
        _do_tokenize(roll_expr, valid_tokens, roll_expr_length, roll_expr_offset + length, [
          token | result
        ])
    end
  end

  defp _match_and_create(_roll_expr, _roll_expr_offset, []) do
    {:error, "No matching token found!"}
  end

  defp _match_and_create(roll_expr, roll_expr_offset, [candidate | other_candidates]) do
    with %{regex: regex} when regex != nil <- candidate,
         matches when matches != nil <-
           Regex.run(regex, roll_expr, offset: roll_expr_offset, return: :index),
         {token, length} when length > 0 <- Rollex.Token.create(candidate, roll_expr, matches) do
      {token, length}
    else
      _ ->
        _match_and_create(roll_expr, roll_expr_offset, other_candidates)
    end
  end
end
