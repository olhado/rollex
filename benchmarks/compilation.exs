benchmark =
  Enum.reduce(
    [
      "1d4+2d6+3d8+4d10+5d12+6d20+40/3",
      "1d4d1+2d6=[2..4]+3d8>2+4d10<3+5d12+6d20+40/3",
      "10d6d1",
      "10d6=[1..4]",
      "10d6>4"
    ],
    %{},
    fn roll, acc ->
      Map.put(acc, roll, fn -> Rollex.compile(roll) end)
    end
  )

config = [memory_time: 2]

Benchee.run(benchmark, config)
