benchmark =
  Enum.reduce(
    ["10d6d1", "10d6=[1..4]", "10d6>4"],
    %{},
    fn roll, acc ->
      compiled = Rollex.compile(roll)
      Map.put(acc, compiled.expression, fn -> Rollex.evaluate(compiled) end)
    end
  )

config = [memory_time: 2]

Benchee.run(benchmark, config)
